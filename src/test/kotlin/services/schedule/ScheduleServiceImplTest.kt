package services.schedule

import io.mockk.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.assertTimeout
import kotlin.system.measureTimeMillis

class ScheduleServiceImplTest {

    @Nested
    inner class `Testing seconds func` {
        @Test
        fun `input more than 0 or equal`() {
            assertEquals(0, ScheduleService.seconds(0))
            assertEquals(1000, ScheduleService.seconds(1))
            assertEquals(60 * 1000, ScheduleService.seconds(60))
            assertEquals(100 * 1000, ScheduleService.seconds(100))
        }

        @Test
        fun `input less than 0`() {
            assertEquals(-1000, ScheduleService.seconds(-1))
            assertEquals(-60 * 1000, ScheduleService.seconds(-60))
            assertEquals(-100 * 1000, ScheduleService.seconds(-100))
        }
    }

    @Nested
    inner class `Testing minutes func` {
        @Test
        fun `input more than 0 or equal`() {
            assertEquals(0, ScheduleService.minutes(0))
            assertEquals(60 * 1000, ScheduleService.minutes(1))
            assertEquals(60 * 60 * 1000, ScheduleService.minutes(60))
            assertEquals(100 * 60 * 1000, ScheduleService.minutes(100))
        }

        @Test
        fun `input less than 0`() {
            assertEquals(-60 * 1000, ScheduleService.minutes(-1))
            assertEquals(-60 * 60 * 1000, ScheduleService.minutes(-60))
            assertEquals(-100 * 60 * 1000, ScheduleService.minutes(-100))
        }
    }

    @Nested
    inner class `Testing hours func` {
        @Test
        fun `input more than 0 or equal`() {
            assertEquals(0, ScheduleService.hours(0))
            assertEquals(60 * 60 * 1000, ScheduleService.hours(1))
            assertEquals(60 * 60 * 60 * 1000, ScheduleService.hours(60))
            assertEquals(100 * 60 * 60 * 1000, ScheduleService.hours(100))
        }

        @Test
        fun `input less than 0`() {
            assertEquals(-60 * 60 * 1000, ScheduleService.hours(-1))
            assertEquals(-60 * 60 * 60 * 1000, ScheduleService.hours(-60))
            assertEquals(-100 * 60 * 60 * 1000, ScheduleService.hours(-100))
        }
    }

    @Nested
    inner class `runSchedule fun` {
        @Test
        fun `It should be 8 - 9 calls for 1 second if we call with delay of 10 millis every 100 millis`() =
            runBlocking {
                val task = mockk<() -> Unit>()
                every { task() } just Runs
                val schedule = ScheduleServiceImpl()

                schedule.runSchedule(10, 100, task)
                delay(1000)
                schedule.stop()
                verify(atLeast = 8, atMost = 11) { task() }
            }

        @Test
        fun `check for ability to be called multiple times`() = runBlocking {
            val task = mockk<() -> Unit>()
            every { task() } just Runs
            val schedule = ScheduleServiceImpl()
            val exception = runCatching {
                repeat(2) {
                    schedule.runSchedule(10, 100, task)
                    delay(500)
                    schedule.stop()
                }
                verify(atLeast = 9, atMost = 11) { task() }
            }
            assert(exception.isSuccess)
        }

        @Test
        fun `stop timer`() {
            val task = mockk<() -> Unit>()
            every { task() } just Runs
            val schedule = ScheduleServiceImpl()
            schedule.runSchedule(1000, 1000, task)
            schedule.stop()
            verify { task wasNot Called }
        }
    }


}