package services.groupServices

import io.mockk.*
import kotlinx.coroutines.runBlocking
import database.models.Group
import database.repositories.groupRepo.GroupRepository
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Nested
import services.client.MephiClient
import services.parser.Parser
import java.time.Instant

class GroupsServiceImplTest {

    @Nested
    inner class Unit {
        private val client = mockk<MephiClient>()
        private val parser = mockk<Parser>()
        private val repo = mockk<GroupRepository>()
        private val db = mutableListOf<Pair<Group, Instant>>()
        private val pageText = """
            <a class="list-group-item text-center text-nowrap" href="/study_groups/14982/schedule">Б22-101
            </a><a class="list-group-item text-center text-nowrap" href="/study_groups/14983/schedule">Б22-102
            </a><a class="list-group-item text-center text-nowrap" href="/study_groups/14984/schedule">Б22-103
            </a><a class="list-group-item text-center text-nowrap" href="/study_groups/14985/schedule">Б22-104
            </a>
                """.trimIndent()
        private val groups = listOf(
            Group("Б22-101", "14982"),
            Group("Б22-102", "14983"),
            Group("Б22-103", "14984"),
            Group("Б22-104", "14985"),
        )

        private fun MockKMatcherScope.iterableEq(seq: Iterable<Group>) = match<Iterable<Group>> {
            it.toList() == seq.toList()
        }

        @Test
        fun downloadAndSaveGroups() = runBlocking {
//            db.clear()
//            val instant = slot<Instant>()
//
//            coEvery { client.getAllGroupsPagesAsText() } returns pageText
//            every { parser.parseGroups(pageText) } returns groups
//            coEvery { repo.saveGroups(iterableEq(groups), capture(instant)) } coAnswers {
//                db += arg<Iterable<Group>>(0).map { it to instant.captured }
//            }
//
//            GroupsServiceImpl(client, parser, repo).downloadAndSaveGroups()
//
//            coVerify {
//                client.getAllGroupsPagesAsText()
//                parser.parseGroups(any())
//                repo.saveGroups(iterableEq(groups), instant.captured)
//            }
//            assert(groups.map { it to instant.captured } == db)
        }

        @Test
        fun deleteGroupsBefore() = runBlocking {
//            db.clear()
//            val instant: Instant = Instant.now()
//            val instantPrev: Instant = instant.minusSeconds(1)
//            db += groups.map { it to instant }
//            db += groups.map { it to instantPrev }
//
//            coEvery { repo.deleteGroupByGroupNumBefore(any(), instant) } coAnswers {
//                db.remove(firstArg<Group>() to instant)
//            }
//
//            GroupsServiceImpl(client, parser, repo).deleteGroupsBefore(groups, instant)
//
//            assert(db == groups.map { it to instantPrev })
        }
    }
}