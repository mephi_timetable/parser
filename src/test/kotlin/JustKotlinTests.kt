
interface Repo {
    var transaction: Transaction?
}
open class Transaction {
    fun inside(vararg repos: Repo, codeBlock: Transaction.() -> Unit) {
        repos.forEach { repo ->
            repo.transaction = this
        }
        this.codeBlock()
    }




}
class A

class Repo1(override var transaction: Transaction? = null) : Transaction(), Repo {
    fun get() = inside {

    }
    fun Transaction.getWithTransaction() {}
}

class Caller(
) {
    init {


        val repo1 = Repo1()
        Transaction().inside(repo1) {
            repo1.get()
        }
        with(Transaction()) {
//            repo1.getWithTransaction()
        }
        repo1.get()
    }
}