package tests

import org.koin.test.KoinTest
import java.io.File

class TimetableUpdate : KoinTest {
    private val groupsPage = File("src/test/resources/studyGroups0.html").readText()
    private val allGroupsPages = File("src/test/resources/allStudyGroups.html").readText()
    private val timetable = File("src/test/resources/groupTimetable.ics").readText()
    private val b22101File = File("src/test/resources/B22_101_timetable.ics").readText()

//    init {
//        startKoin {
//            modules(module {
//                singleOf(::GroupRepositoryImpl) { bind<GroupRepository>() }
//                singleOf(::TimetableRepositoryImpl) { bind<TimetableRepository>() }
//                singleOf(::ParserImpl) { bind<Parser>() }
//                singleOf(::Database) withOptions { createdAtStart() }
//                singleOf(::MephiClientImpl)
//                single<TimetableService> {
//                    TimetableServiceImpl(get(), get(), get(), get(), 2)
//                }
//                singleOf(::GroupsServiceImpl)
//                singleOf(::InnerGroupRepoImpl) { bind<InnerGroupRepo>() }
//
//            })
//        }
//    }

//    @Test
//    fun updateTimetable() = runBlocking {
//        val parser: Parser by inject()
//        val groupRepository: GroupRepository by inject()
//        val timetableRepository: TimetableRepository by inject()
//        val group = Group("Б22-101", "12345")
//        val instant = Instant.now()
//        val list = parser.parseGroupTimetable(
//            b22101File,
//            group
//        )
//        groupRepository.saveGroups(listOf(group), instant)
//        val flow = channelFlow<Lesson> {
//            list.forEach { send(it) }
//        }
//        timetableRepository.updateTimetable(flow, instant)
//        groupRepository.deleteGroupByGroupNumBefore(group, instant)
//    }
}