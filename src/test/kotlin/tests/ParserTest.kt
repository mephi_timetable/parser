package tests

import kotlinx.coroutines.runBlocking
import net.fortuna.ical4j.data.CalendarBuilder
import net.fortuna.ical4j.model.Component
import net.fortuna.ical4j.model.Property
import net.fortuna.ical4j.model.component.VEvent
import net.fortuna.ical4j.model.property.RRule
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import services.client.MephiClientImpl
import services.parser.ParserImpl
import java.io.File
import java.io.FileInputStream


class ParserTest {
    val parser = ParserImpl()

    @Nested
    inner class Offline {
        private val groupsPage = File("src/test/resources/studyGroups0.html").readText()
        private val allGroupsPages = File("src/test/resources/allStudyGroups.html").readText()
        private val timetable = File("src/test/resources/groupTimetable.ics").readText()
        private val b22101File = File("src/test/resources/B22_101_timetable.ics").readText()



        @Test
        fun `check size of bachelors groups`() = assertDoesNotThrow {
            val groups = parser.parseGroups(groupsPage)
            val numOfGroups = groups.size
            println("Number of bachelors groups: $numOfGroups")
            assert(numOfGroups == 219)
        }

        @Test
        fun `check size of all groups`() = assertDoesNotThrow {
            val groups = parser.parseGroups(allGroupsPages)
            val numOfGroups = groups.size
            println("Number of bachelors groups: $numOfGroups")
            assert(numOfGroups == 626)
        }

        @Test
        fun `check groupNum len`() {
            val groups = parser.parseGroups(allGroupsPages)
            parser.getGroupsNums(groups).map { it.length }.toSet().also { println(it) }
        }

        @Test
        fun `timetable`() {
            println(parser.parseGroupTimetable(timetable))
        }

        @Test
        fun `B22-101 testing`() {
            parser.parseGroupTimetable(b22101File)
        }
    }

    @Nested
    inner class Online {
        val MephiClient = MephiClientImpl()

        @Test
        fun `parse b21-503 group`() = runBlocking {
            val text = MephiClient.getMyGroupTimetable()
            println(parser.parseGroupTimetable(text))
        }
    }
    @Test
    fun `parse file`() {
        val fin = FileInputStream("src/test/resources/groupTimetable.ics")
        val builder = CalendarBuilder()
        val calendar = builder.build(fin)
        val events = calendar.getComponents<VEvent>(Component.VEVENT)
        for (event in events) {
            println("-----------")
            val summary = event.summary.value
            val description = event.description.value
            val place = event.location.value
            val recur = event.getProperties<RRule>(Property.RRULE).last().recur
            val lessonStart = event.startDate.value
            val lessonEnd = event.endDate.value
            val lastLesson = recur.until
            val lessonFrequency = recur.frequency
            val lessonInterval = recur.interval
            println(summary)
            println(description)
            println(place)
            println(lessonStart)
            println(lessonEnd)
            println(lastLesson)
            println(lessonFrequency)
            println(lessonInterval)
            println("-----------")
        }
    }

    @Test
    fun `time table parse test`() {
        val fin = FileInputStream("src/test/resources/groupTimetable.ics")
        val builder = CalendarBuilder()
        val calendar = builder.build(fin)
        val events = calendar.getComponents<VEvent>(Component.VEVENT)
        println(events)
        events.first().let { event ->
//            println(LocalDateTime.parse(event.startDate.value,
//                DateTimeFormatter.ofPattern("yyyyMMdd'T'HHmmss")))
            println("-----------")
            val recur = event.getProperties<RRule>(Property.RRULE).last().recur
            val lessonStart = event.startDate.value
            val lessonEnd = event.endDate.value
            val lastLesson = recur.until
            val lessonFrequency = recur.frequency
            val lessonInterval = recur.interval
            println(lessonStart)
            println(lessonEnd)
            println(lastLesson)
            println(lessonFrequency)
            println(lessonInterval)
            println("-----------")


//            val from = DateTime(event.startDate.value)
////            println("${from.toLocalDateTime()}, $to")
//
//            println(recur.interval)
//            val to = DateTime(recur.until)
//
//            val period = Period(from, to)
//            val list = event.calculateRecurrenceSet(period)
//            println("Perion ${list.map { it.rangeEnd.toLocalDateTime() }}")
        }
    }
}
