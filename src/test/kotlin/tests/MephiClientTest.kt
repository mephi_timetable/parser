package tests

import kotlinx.coroutines.runBlocking
import database.models.Group
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import services.client.MephiClientImpl
import java.io.File

class MephiClientTest {
    private val mephiClient = MephiClientImpl()
    private val groupB21503 = Group("Б21-503", "14953")

    @Test
    fun `get bachelors groups page`() = assertDoesNotThrow {
        runBlocking { mephiClient.getGroupsPageAsText().also { println(it) } }
    }

    @Test
    fun `get all groups pages`() = assertDoesNotThrow {
        runBlocking { mephiClient.getAllGroupsPagesAsText().also { println(it) } }
    }

    @Test
    fun `get 14953 (b21-503) timetable`() = assertDoesNotThrow {
        runBlocking { mephiClient.getMyGroupTimetable().also { println(it) } }
    }

    @Test
    fun `get group timetable`() = assertDoesNotThrow {
        runBlocking { mephiClient.getGroupTimetable(groupB21503).also { println(it) } }
    }

    //    @Nested
    inner class UpdateFiles {
        @Test
        fun `update allStudyGroups_html`() {
            val file = File("src/test/resources/allStudyGroups.html")
            runBlocking { mephiClient.getAllGroupsPagesAsText() }.also {
                file.writeText(it)
            }
        }

        @Test
        fun `update studyGroups0_html`() {
            val file = File("src/test/resources/studyGroups0.html")
            runBlocking { mephiClient.getAllGroupsPagesAsText() }.also {
                file.writeText(it)
            }
        }
    }


}