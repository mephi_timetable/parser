package tests

import configurations.Database
import kotlinx.coroutines.runBlocking
import database.models.Group
import database.repositories.groupRepo.GroupRepository
import database.repositories.timetableRepo.TimetableRepository
import org.junit.jupiter.api.Test
import org.koin.test.KoinTest
import org.koin.test.inject
import services.groupServices.GroupsServiceImpl
import services.client.MephiClientImpl
import services.parser.ParserImpl
import services.timetable.TimetableService

class TimetableServiceTest : KoinTest {
    init {
//        startKoin {
//            modules(module {
//                singleOf(::GroupRepositoryImpl) { bind<GroupRepository>() }
//                singleOf(::TimetableRepositoryImpl) { bind<TimetableRepository>() }
//                singleOf(::ParserImpl) { bind<Parser>() }
//                singleOf(::Database) withOptions { createdAtStart() }
//                singleOf(::MephiClientImpl)
//                single<TimetableService> {
//                    TimetableServiceImpl(get(), get(), get(), get(), 2)
//                }
//                singleOf(::GroupsServiceImpl)
//            })
//        }
    }

    val groups = listOf<Group>(
        Group("Б22-101", "14982"),
        Group("Б22-102", "14983"),
        Group("Б22-103", "14984"),
        Group("Б22-104", "14985"),
    )
    val groupRepository = inject<GroupRepository>()
    val timetableRepository = inject<TimetableRepository>()
    val client = inject<MephiClientImpl>()
    val parser = inject<ParserImpl>()
    val database = inject<Database>()
    val timetableService = inject<TimetableService>()
    val groupsService = inject<GroupsServiceImpl>()

    @Test
    fun `saving timetable1`() = runBlocking {
//        val instant = Instant.now()
//        groupRepository.value.saveGroups(groups, instant)
//        timetableService.value.update(groups)
//        groups.forEach { group ->
//            try {
//                groupRepository.value.deleteGroupByGroupNumBefore(group, instant)
//            } catch (exception: ExposedSQLException) {
//                println(exception)
//            }
//        }

    }

    @Test
    fun `saving timetable`() = runBlocking {
//        groupRepository.value.update(groups, Instant.now())
//        val flow = channelFlow<Pair<Group, Lesson>> {
//            val chunkedList = groups.chunked(2)
//            println("Chunks size: ${chunkedList.size}")
//            chunkedList.forEachIndexed { index, chunk ->
//                println("\tChunk $index")
//                coroutineScope {
//                    chunk.forEach { group ->
//                        println("\tfor group ${group.groupNum}")
//                        launch {
//                            val text = client.value.getGroupTimetable(group)
//                            val lessons = parser.value.parseGroupTimetable(text)
//                            println("\t\tList of lessons created")
//                            lessons.forEach { lesson ->
//                                send(group to lesson)
//                            }
//                        }
//                    }
//                }
//                println("\tchunk $index done")
//            }
//        }
//        timetableRepository.value.updateTimetable(flow, Instant.now())
    }
}