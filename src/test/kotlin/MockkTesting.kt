import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import junit.framework.TestCase.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow

class Dependency1(val value1: Int) {
    fun call(arg: Int) = 10
}
class Dependency2(val value2: String)

class SystemUnderTest(
    val dependency1: Dependency1,
    val dependency2: Dependency2
) {
    fun calculate(inp: Int) =
        dependency1.value1 + dependency2.value2.toInt() + dependency1.call(inp)
}

class MockkTesting {
    @Test
    fun calculateAddsValues() {
        val doc1 = mockk<Dependency1>()
        val doc2 = mockk<Dependency2>()

        every { doc1.value1 } returns 5
        every { doc2.value2 } returns "6"

        val capture = mutableListOf<Int>()
        every { doc1.call(capture(capture)) } answers { arg(0) }
//        every { doc1.call(more(5)) } returns 1
//        every { doc1.call(or(less(5), 5)) } returns -1
//        every { doc1.call(any()) } returnsMany listOf(1, 2, 3)


        val sut = SystemUnderTest(doc1, doc2)

        assertDoesNotThrow { sut.calculate(12) }
        println(capture)

        verify {
            doc1.call(any())
        }

        println(sut.calculate(6))
        println(capture)
        println(sut.calculate(3))
        println(sut.calculate(1))
        println(sut.calculate(1))

    }
}