package controllers

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import services.groupServices.GroupsService
import services.schedule.ScheduleServiceImpl
import services.timetable.TimetableService
import java.time.Instant
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.CoroutineScope
import services.schedule.ScheduleService

private val logger = KotlinLogging.logger {}

class MainClass : KoinComponent {
    private val groupsService: GroupsService by inject()
    private val scheduleService: ScheduleServiceImpl by inject()
    private val timetableService: TimetableService by inject()

//    fun start(coroutineScope: CoroutineScope) {
//        scheduleService.runSchedule(0, ScheduleService.hours(2)) {
//            logger.info("Schedule service started")
//            coroutineScope.launch(Dispatchers.IO) {
//                val instant = Instant.now()
//                val groups = groupsService.downloadAndSaveGroups().toList()
//                timetableService.update(groups)
//                groupsService.deleteGroupsBefore(groups, instant)
//            }
//        }
//    }
fun start(coroutineScope: CoroutineScope) {
    scheduleService.runSchedule(0, ScheduleService.hours(2)) {
        logger.info("Schedule service started")
        coroutineScope.launch(Dispatchers.IO) {
            val groups = groupsService.downloadGroups().toList()
            timetableService.update(groups)
        }
    }
}

//    fun printMyGroup() {
//        coroutineScopeProvider.coroutineScope.launch(Dispatchers.IO) {
//            timetableService.getMyGroupLessons()
//        }
//    }
}