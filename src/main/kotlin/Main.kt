import configurations.sharedModule
import controllers.MainClass
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.core.context.startKoin


fun main(args: Array<String>) = runBlocking {
    val coroutineScope = this
    startKoin {
        modules(sharedModule())
    }
//    println("Started")
    launch {
        while(true) delay(1000)
    }
    MainClass().start(coroutineScope)
    Unit
}