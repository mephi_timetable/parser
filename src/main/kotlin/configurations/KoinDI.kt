package configurations

import database.repositories.TimetableRepo
import database.repositories.TimetableRepoImpl
import database.repositories.groupRepo.GroupRepository
import database.repositories.groupRepo.GroupRepositoryImpl
import database.repositories.timetableRepo.TimetableRepository
import database.repositories.timetableRepo.TimetableRepositoryImpl
import org.koin.core.module.dsl.*
import org.koin.dsl.module
import services.client.MephiClient
import services.client.MephiClientImpl
import services.groupServices.GroupsService
import services.groupServices.GroupsServiceImpl
import services.parser.Parser
import services.parser.ParserImpl
import services.schedule.ScheduleService
import services.schedule.ScheduleServiceImpl
import services.timetable.TimetableService
import services.timetable.TimetableServiceImpl

fun sharedModule() = module {
    // configurations
    singleOf(::Database) { createdAtStart() }
    // repos
    singleOf(::GroupRepositoryImpl) { bind<GroupRepository>() }
    singleOf(::TimetableRepositoryImpl) { bind<TimetableRepository>() }
    singleOf(::TimetableRepoImpl) { bind<TimetableRepo>() }
    // services
    singleOf(::ScheduleServiceImpl) { bind<ScheduleService>(); createdAtStart() }
    singleOf(::ParserImpl) { bind<Parser>() }
    singleOf(::GroupsServiceImpl) { bind<GroupsService>() }
    single { TimetableServiceImpl(get(), get(), get()) } withOptions { bind<TimetableService>() }
    singleOf(::MephiClientImpl) {
        createdAtStart()
        bind<MephiClient>()
    } withOptions {
        onClose { it?.close() }
    }

}