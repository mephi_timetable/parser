package services.schedule

import io.github.oshai.kotlinlogging.KotlinLogging
import java.util.*

private val logger = KotlinLogging.logger {}

class ScheduleServiceImpl : ScheduleService {
    private var timer = Timer()
    override fun runSchedule(delay: Long, runEveryMillis: Long, task: () -> Unit) {
        val timerTask = object : TimerTask() {
            override fun run() {
                logger.info("Task started")
                task()
            }
        }
        timer.schedule(timerTask, delay, runEveryMillis)
    }

    override fun stop() {
        timer.cancel()
        timer = Timer()
    }
}