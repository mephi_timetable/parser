package services.schedule

interface ScheduleService {
    companion object {
        fun seconds(n: Long) = n * 1000
        fun minutes(n: Long) = seconds(n) * 60
        fun hours(n: Long) = minutes(n) * 60
    }

    fun runSchedule(delay: Long, runEveryMillis: Long, task: () -> Unit)

    fun stop()
}