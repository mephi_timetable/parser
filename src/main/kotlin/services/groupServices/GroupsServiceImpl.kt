package services.groupServices

import io.github.oshai.kotlinlogging.KotlinLogging
import database.models.Group
import org.koin.core.component.KoinComponent
import services.client.MephiClient
import services.parser.Parser

private val logger = KotlinLogging.logger {}


class GroupsServiceImpl(
    private val client: MephiClient,
    private val parser: Parser,
) : KoinComponent, GroupsService {

    override suspend fun downloadGroups(): Set<Group> {
        val text = client.getAllGroupsPagesAsText()
        return parser.parseGroups(text).toSet()
    }
}