package services.groupServices

import database.models.Group
import java.time.Instant

interface GroupsService {
//    suspend fun downloadAndSaveGroups(): Set<Group>
//    suspend fun deleteGroupsBefore(groups: List<Group>, instant: Instant)
    suspend fun downloadGroups(): Set<Group>

}