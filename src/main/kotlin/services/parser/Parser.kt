package services.parser

import database.models.Group
import database.models.Lesson
import net.fortuna.ical4j.data.CalendarBuilder
import net.fortuna.ical4j.model.Calendar
import net.fortuna.ical4j.model.Component
import net.fortuna.ical4j.model.Property
import net.fortuna.ical4j.model.component.VEvent
import net.fortuna.ical4j.model.property.RRule
import java.io.StringReader

interface Parser {
    fun parseGroups(text: String): List<Group>
    fun getGroupsNums(groups: List<Group>): Set<String>
    fun parseGroupTimetable(text: String, group: Group? = null): List<Lesson>
}