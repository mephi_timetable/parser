package services.parser

import database.models.Group
import database.models.Lesson
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.datetime.Clock
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import net.fortuna.ical4j.data.CalendarBuilder
import net.fortuna.ical4j.model.Calendar
import net.fortuna.ical4j.model.Component
import net.fortuna.ical4j.model.Property
import net.fortuna.ical4j.model.component.VEvent
import net.fortuna.ical4j.model.property.RRule
import java.io.StringReader
import java.time.format.DateTimeFormatter

private val logger = KotlinLogging.logger {}

class ParserImpl : Parser {
    private val regex = """"/study_groups/\d*/schedule">...-...""".toRegex()
//    private val mephiRootPath = "https://home.mephi.ru"

    override fun parseGroups(text: String): List<Group> {
        val listOfStrings = regex.findAll(text).toList().map { it.value }
//        val groupsNums = listOfStrings.map { it.substringAfter('>') }
        val groups = listOfStrings.map {
            val groupNum = it.substringAfter('>')
            val groupRef = it.substringAfter("study_groups/").substringBefore('/')
            Group(groupNum, groupRef)
        }
        return groups
    }

    override fun getGroupsNums(groups: List<Group>) = groups.map { it.groupNum }.toSet()

    private fun String?.icalDateTimeToLocalDateTime() = this?.let {
        runCatching {
            java.time.LocalDateTime.parse(
                this,
                DateTimeFormatter.ofPattern("yyyyMMdd'T'HHmmss")
            ).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME).toLocalDateTime()
        }.getOrNull() ?: runCatching {
            java.time.LocalDateTime.parse(
                this,
                DateTimeFormatter.ofPattern("yyyyMMdd'T'HHmmss'Z'")
            ).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME).toLocalDateTime()
        }.getOrNull()
    }


    override fun parseGroupTimetable(text: String, group: Group?): List<Lesson> {
        val string = StringReader(text)
        val calendarBuilder = CalendarBuilder()
        val calendar: Calendar = calendarBuilder.build(string)
        val listEvent = calendar.getComponents<VEvent>(Component.VEVENT)
        val timetable = mutableListOf<Lesson>()
        for (event in listEvent) {
            val recur = try {
                event.getProperties<RRule>(Property.RRULE).last().recur
            } catch (ex: Exception) {
                null
            }
            try {
                timetable += Lesson(
                    group = group,
                    discipline = event?.summary?.value,
                    teacher = event?.description?.value,
                    place = event?.location?.value,
                    lessonStart = event?.endDate?.value?.icalDateTimeToLocalDateTime()
                        ?: throw DateTimeNullException("Lesson has no start"),
                    lessonEnd = event.endDate?.value?.icalDateTimeToLocalDateTime()
                        ?: throw DateTimeNullException("Lesson has no end"),
                    until = recur?.until?.toString()?.icalDateTimeToLocalDateTime(),
                    repeat = recur?.frequency?.toString(),
                    interval = recur?.interval ?: 1,
                    downloadedTime = Clock.System.now().toLocalDateTime(TimeZone.currentSystemDefault())
                )
            } catch (ex: DateTimeNullException) {
                logger.warn(ex.message)
            }
        }
        return timetable.toList()
    }

    class DateTimeNullException(message: String): RuntimeException(message)
}