package services.timetable

import database.models.Group
import database.models.Lesson

interface TimetableService {
    suspend fun update(list: List<Group>)
}