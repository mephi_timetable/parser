package services.timetable

import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.channels.ProducerScope
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.launch
import database.models.Group
import database.models.Lesson
import database.repositories.TimetableRepo
import services.client.MephiClientImpl
import services.parser.ParserImpl
import java.time.Instant

private val logger = KotlinLogging.logger {}

class TimetableServiceImpl(
    private val parser: ParserImpl,
    private val client: MephiClientImpl,
    private val timetableRepo: TimetableRepo,
    private val chunkSize: Int = 10,
    ) : TimetableService {

override suspend fun update(list: List<Group>) {
    channelFlow {
        val chunkedList = list.chunked(chunkSize)
        logger.info("Chunks size: ${chunkedList.size}")
        chunkedList.forEachIndexed { index, chunk ->
            sendLessons(chunk)
            logger.info("chunk ${index + 1} done")
        }
    }.collect { (group, lessons) ->
        timetableRepo.updateGroupWithLessons(group, lessons, Instant.now())
    }
}

    private suspend fun ProducerScope<Pair<Group, List<Lesson>>>.sendLessons(chunk: List<Group>) =
        coroutineScope {
            chunk.forEach { group ->
                launch {
                    try {
                        val text = client.getGroupTimetable(group)
                        val lessons = parser.parseGroupTimetable(text, group)
                        send(group to lessons)
                    } catch (exception: Exception) {
                        logger.error("Lessons for ${group.groupNum} were not downloaded. $exception")
                    }
                }
            }
        }
}