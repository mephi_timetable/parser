package services.client

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import database.models.Group

interface MephiClient {
    suspend fun getGroupsPageAsText(): String
    suspend fun getAllGroupsPagesAsText(): String

    suspend fun getMyGroupTimetable(): String

    suspend fun getGroupTimetable(group: Group) : String

    fun close()
}