package services.client

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import database.models.Group

class MephiClientImpl : MephiClient {
    private val client = HttpClient(CIO) {
        install(HttpTimeout) {
            requestTimeoutMillis = 1000 * 10 // 10 sec
        }
    }
    private val groupsLink = IntRange(0, 4).map {
        "https://home.mephi.ru/study_groups?level=$it&organization_id=1&term_id=16"
    }
    override suspend fun getGroupsPageAsText() =
        client.get("https://home.mephi.ru/study_groups?level=0&organization_id=1&term_id=16").bodyAsText()

    override suspend fun getAllGroupsPagesAsText() =
        groupsLink.map { client.get(it).bodyAsText() }.joinToString("\n")

    override suspend fun getMyGroupTimetable() =
        client.get("https://home.mephi.ru/study_groups/14953/schedule.ics").bodyAsText()

    override suspend fun getGroupTimetable(group: Group) =
        client.get("https://home.mephi.ru/study_groups/${group.groupRefId}/schedule.ics").bodyAsText()

    override fun close() = client.close()
}