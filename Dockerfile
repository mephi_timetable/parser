FROM eclipse-temurin:11-jre
LABEL authors="alex"

RUN mkdir /app
COPY ./build/distributions/*SNAPSHOT.tar /app/parser.tar
RUN tar -xf /app/parser.tar -C /app/
CMD /app/*SNAPSHOT/bin/parser
#ENTRYPOINT ["tail", "-f", "/dev/null"]